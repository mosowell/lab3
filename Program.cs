﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace regexLab
{
    class Program
    {
        static void Main(string[] args)
        {

            string repeatWords = "Задача на нахождение нахождение повторений в строке строке.";
            Console.WriteLine(repeatWords);
            Console.WriteLine(
                Regex.Replace(
                    repeatWords,
                    @"(?<word>\w+)\s(\k<word>)\W",
                    "${word} (повтор '${word})' "
                )
            );

            string unseteNumbers = "34524!@ef3@424D@#42435345fSDFsdf";
            Console.WriteLine();
            Console.WriteLine(unseteNumbers);
            Console.WriteLine(
                Regex.Replace(
                    unseteNumbers,
                    @"\d*",
                    ""
                )
            );

            string unsetMr = "Mr. Don hello. Mrs. Liza. Miss. Missala, Ms. Pops";
            Console.WriteLine();
            Console.WriteLine(unsetMr);
            Console.WriteLine(
                Regex.Replace(
                    unsetMr,
                    @"(Mr|Mrs|Miss|Ms)\.\s",
                    ""
                )
            );


            Regex regex;
            bool hasError = true;
            string emailAddress = "";

            while (hasError)
            {
                Console.WriteLine();
                Console.Write("Введите адрес вашего почтового ящика: ");
                emailAddress = Console.ReadLine();
                
                try
                {
                    regex = new Regex(@"\s");
                    if (regex.IsMatch(emailAddress))
                    {
                        throw new Exception("Недопустимы символы пробела");
                    }

                    regex = new Regex(@"@");
                    if (!regex.IsMatch(emailAddress) || regex.Matches(emailAddress).Count > 1)
                    {
                        throw new Exception("Необходим один символ '@'");
                    }

                    regex = new Regex(@"@[A-zА-яЁё]+\.[A-zА-яЁё]{2,}$");
                    if (!regex.IsMatch(emailAddress) || regex.Matches(emailAddress).Count > 1)
                    {
                        throw new Exception("Неверное задана правая часть email относительно '@'");
                    }

                    regex = new Regex(@"(\w*\.\w*){2,}@");
                    if (regex.IsMatch(emailAddress))
                    {
                        throw new Exception("Недопустимо использовать 2 точки в левой части email относительно '@'");
                    }

                    hasError = false;

                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message + "!");
                    Console.ResetColor();
                }
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine($"{emailAddress} - полностью корректен.");
            Console.ResetColor();
        }
    }
}
